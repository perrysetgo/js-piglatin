  function createCharMap(inputString)
  {
    var chars = inputString.split("");
    return chars;
  }

  function isVowel(chars){
    if ('aeiou'.indexOf(chars[0].toLowerCase()) != -1){ //if the first char is a vowel??
      return true;
    }
    else{
      return false;
    }
  }

  function moveConsonants(chars){

    var arrayLength = chars.length;

    if (isVowel(chars)==true){
      chars.push("a","y");
      return chars;
    }

    else {
      //debugger;
      for(var i = 0; i < arrayLength && !isVowel(chars); i++)
      {
        console.log("I: " + i);
        //debugger;
        if('aeiou'.indexOf(chars[i].toLowerCase()) == -1){
          chars.push(chars[i]);
          //chars.shift();
          chars.splice(i,1);
        }
      }
      chars.push("a", "y");
    }
    return chars;
  }



  function makeNewWord(chars){
    var newWord = chars.toString();
    newWord = newWord.replace(/,/g , "");
    return newWord;
  }

$(document).ready(function() {
  $("form#piglatin").submit(function(event) {
    inputString = $("input#input1").val();

    var chars = createCharMap(inputString);
    var newChars = moveConsonants(chars);
    var newWord = makeNewWord(newChars);

      $(".answer").html(newWord);

    $("#result").show();
    event.preventDefault();
  });
});
